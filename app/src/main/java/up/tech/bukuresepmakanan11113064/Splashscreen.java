package up.tech.bukuresepmakanan11113064;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.Toast;

import com.facebook.stetho.Stetho;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import up.tech.bukuresepmakanan11113064.API.POJOCategory;
import up.tech.bukuresepmakanan11113064.API.POJOResep;
import up.tech.bukuresepmakanan11113064.API.RESTClient;
import up.tech.bukuresepmakanan11113064.Database.Controller.CategoryController;
import up.tech.bukuresepmakanan11113064.Database.Controller.ResepMakananController;
import up.tech.bukuresepmakanan11113064.Database.Model.CategoryModel;
import up.tech.bukuresepmakanan11113064.Database.Model.ResepMakanan;

public class Splashscreen extends AppCompatActivity {

    Context myContext;
    ProgressDialog progress;
    SharedPreferences pref ;
     SharedPreferences.Editor editor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splashscreen);
        pref = getSharedPreferences("URLAPI", MODE_PRIVATE);


        final int idcategory = 1;
//        final String  namePref= "succes";
//        final boolean[] satu = {false,false};
//        boolean dua =false;
        myContext = getApplicationContext();


        Stetho.initializeWithDefaults(myContext);
        final CategoryController categoryDB = new CategoryController(myContext);
        final  ResepMakananController resepController= new ResepMakananController(myContext);
//        progress = ProgressDialog. show(Splashscreen. this,
//                "Inisialisasi Data", "Sedang Mengunduh Data Untuk Aplikasi" ,
//                true);



         fade();
         final Intent mainIntent =  new Intent(getApplicationContext(), LoginActivity.class);
         RESTClient.get().getAllCategory(new Callback<POJOCategory>() {
             @Override
             public void success(POJOCategory pojoCategory, Response response) {
                if(pojoCategory.getResepbycategory().size()>0) {
                    try {
                        categoryDB.open();
                        categoryDB.deleteData();

                        for (int i = 0; i < pojoCategory.getResepbycategory().size(); i++) {
                            CategoryModel model = new CategoryModel();
                            model = pojoCategory.getResepbycategory().get(i);
                            categoryDB.insertData(model.getId_category(), model.getNama_category());
                        }


                    }
                      catch (Exception e) {
                            Log.e("Erorr", "OPEN DATABASE", e);
                      }
                      finally {
                        ArrayList<CategoryModel> test = categoryDB.getData();
                        Log.i("DATA RESEP", "CATEGORY TOTAL "+ test.size());
                          categoryDB.close();
                      }

                }
                 else {
                    Toast.makeText(myContext, "DATA NOT READY", Toast.LENGTH_LONG).show();
                }

             }

             @Override
             public void failure(RetrofitError error) {
                Log.e("Error", "RETROFIT CATEGORY", error);
             }
         });

        RESTClient.get().getAllResep(new Callback<POJOResep>() {
            @Override
            public void success(POJOResep pojoResep, Response response) {

                if(pojoResep.getResepbycategory().size()>0){
                    try {
                        resepController.open();
                        resepController.deleteData();
                        for (int i =0; i< pojoResep.getResepbycategory().size(); i++){
                            ResepMakanan mkanan = new ResepMakanan();
                            mkanan = pojoResep.getResepbycategory().get(i);
                            resepController.insertData(mkanan.getId_resep(),
                                    mkanan.getId_category(),mkanan.getNama(),
                                    mkanan.getDeskripsi(),mkanan.getBahan(),
                                    mkanan.getCara_Membuat(), mkanan.getImage());
                        }

                    }
                    catch ( Exception e) {
                        Log.e("ERROR", "DATABASE", e);
                    }
                    finally {

                        ArrayList<ResepMakanan> test = resepController.getData();
                        Log.i("DATA RESEP", "RESEP TOTAL "+ test.size());
                        resepController.close();
                    }

                }
                else {
                    Toast.makeText(myContext, "DATA NOT READY", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("Error", "RETROFIT RESEP", error);
            }
        });

        editor = pref.edit();
        editor.putString("url",RESTClient.BASE_URL);
        editor.commit();

        Runnable r= new Runnable() {
            @Override
            public void run() {
                finish();
                startActivity(mainIntent);
            }
        };
        Handler hand = new Handler();
        hand.postDelayed(r,3000);

    }
    public void blink() {
        ImageView imageView = (ImageView) findViewById(R.id.imageView);
        Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.blink);
        imageView.startAnimation(animation);

    }
    public void slide() {
        ImageView imageView = (ImageView) findViewById(R.id.imageView);
        Animation animation = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.slide);
        imageView.startAnimation(animation);

    }
    public void fade() {
        ImageView imageView = (ImageView) findViewById(R.id.imageView);
        Animation animation = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.fade);
        imageView.startAnimation(animation);

    }
}
