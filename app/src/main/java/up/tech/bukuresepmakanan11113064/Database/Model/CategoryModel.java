package up.tech.bukuresepmakanan11113064.Database.Model;

/**
 * Created by Takiya on 10/05/2016.
 */
public class CategoryModel {
    int id_category;
    String nama_category;

    public int getId_category() {
        return id_category;
    }

    public void setId_category(int id_category) {
        this.id_category = id_category;
    }

    public String getNama_category() {
        return nama_category;
    }

    public void setNama_category(String nama_category) {
        this.nama_category = nama_category;
    }

    @Override
    public String toString() {
        return "CategoryModel{" +
                "id_category=" + id_category +
                ", nama_category='" + nama_category + '\'' +
                '}';
    }
}
