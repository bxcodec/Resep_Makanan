package up.tech.bukuresepmakanan11113064.Database.Model;

/**
 * Created by Takiya on 12/04/2016.
 */
public class Saran {
    int id;
    String judul;
    String saran;

//    public Saran(int id, String username, String saran) {
//        this.id = id;
//        this.username = username;
//        this.saran = saran;
//    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getSaran() {
        return saran;
    }

    public void setSaran(String saran) {
        this.saran = saran;
    }
}
