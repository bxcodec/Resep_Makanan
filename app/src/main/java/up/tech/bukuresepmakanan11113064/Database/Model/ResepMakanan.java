package up.tech.bukuresepmakanan11113064.Database.Model;

import java.io.Serializable;

/**
 * Created by Takiya on 30/04/2016.
 */public class ResepMakanan  implements Serializable{
    int id_resep;
    int id_category;
    String Nama;
    String Deskripsi;
    String Bahan;
    String Cara_Membuat;
    String image;

    public int getId_category() {
        return id_category;
    }

    public void setId_category(int id_category) {
        this.id_category = id_category;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getId_resep() {
        return id_resep;
    }

    public void setId_resep(int id_resep) {
        this.id_resep = id_resep;
    }

    public String getNama() {
        return Nama;
    }

    public void setNama(String nama) {
        Nama = nama;
    }

    public String getDeskripsi() {
        return Deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        Deskripsi = deskripsi;
    }

    public String getBahan() {
        return Bahan;
    }

    public void setBahan(String bahan) {
        Bahan = bahan;
    }

    public String getCara_Membuat() {
        return Cara_Membuat;
    }

    public void setCara_Membuat(String cara_Membuat) {
        Cara_Membuat = cara_Membuat;
    }
}
