package up.tech.bukuresepmakanan11113064.Database.Controller;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import up.tech.bukuresepmakanan11113064.Database.Model.CategoryModel;
//import up.tech.bukuresepmakanan11113064.Database.Model.ResepMakanan;

/**
 * Created by Takiya on 10/05/2016.
 */
public class CategoryController {

        DbController dbHelper;
        SQLiteDatabase database;
        public static final String TABLE_NAME = "category";
        public static final String ID = "id_category";
        //    public static final String USER = "username";
//    public static final String SARAN = "saran";
//    int id;
//    public static final String category="category";
        public static final String Nama ="nama_category";
        public  static  String CREATE_TABEL_CATEGORY= "create table "
                +TABLE_NAME + " (" + ID+ " integer primary key, "
                + Nama+ " text)";


        public  String [] tblColoumn = {ID,Nama};



        public CategoryController (Context c) {
            dbHelper = new DbController(c);

        }
        public void  open() throws  Exception{

            database= dbHelper.getWritableDatabase();

        }

        public  void close() {
            dbHelper.close();
        }

        public void deleteData() {
            database.delete(TABLE_NAME, null, null);
        }

        public  void insertData( int id , String nama) {
            ContentValues contentValues =  new ContentValues();
            contentValues.put(ID,id);
//        contentValues.put(category,cate);
            contentValues.put(Nama,nama);

            database.insert(TABLE_NAME, null, contentValues);

        }


        public CategoryModel getCategoryByName(String nama) {

            CategoryModel model = new CategoryModel();
            Cursor cursor = null;
            cursor = database.query(TABLE_NAME,tblColoumn, Nama+"=?",new String[]{nama},null,null,null);

            if(cursor.getCount()>0) {
                cursor.moveToFirst();
                model = parseData(cursor);
            }

//            while (!cursor.isAfterLast())



            cursor.close();
//            return allData;

            return model;

        }

        public ArrayList<CategoryModel> getData() {


            ArrayList<CategoryModel> allData = new ArrayList<CategoryModel>();
            Cursor cursor = null;
            cursor = database.query(TABLE_NAME, tblColoumn, null, null, null,null, ID + " ASC");
            cursor.moveToFirst();

            while (!cursor.isAfterLast()) {
                allData.add(parseData(cursor));
                cursor.moveToNext();
            }

            cursor.close();
            return allData;

        }


        private CategoryModel parseData(Cursor cursor) {
            CategoryModel curData = new CategoryModel();
            curData.setId_category(cursor.getInt(0));
            curData.setNama_category(cursor.getString(1));
//            curData.setDeskripsi(cursor.getString(2));
//            curData.setBahan(cursor.getString(3));
//            curData.setCara_Membuat(cursor.getString(4));
            return curData;
        }

    }


