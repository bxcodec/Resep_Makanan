package up.tech.bukuresepmakanan11113064.Database.Controller;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

import up.tech.bukuresepmakanan11113064.Database.Model.ResepMakanan;
//import up.tech.bukuresepmakanan11113064.Database.Model.Saran;

/**
 * Created by Takiya on 30/04/2016.
 */
public class ResepMakananController {
    DbController dbHelper;
    SQLiteDatabase database;
    public static final String TABLE_NAME = "resepMakanan";
    public static final String ID = "id";
//    public static final String USER = "username";
//    public static final String SARAN = "saran";
//    int id;
//    public static final String category="category";
    public static final String Nama ="nama";
    public static final String Deskripsi ="description" ;
    public static final String Bahan="bahan";
    public static final String Cara_Membuat="cara_membuat";
    public  static final String IdCategory = "id_category";
    public  static  final String Image = "Image";


    public  static  String CREATE_TABEL_RESEP= "create table "
            +TABLE_NAME + " (" + ID+ " integer primary key, " + IdCategory + " integer, "
            + Nama+ " text," + Deskripsi + " text,"+ Bahan+ " text,"  + Cara_Membuat+" text, "+ Image + " text )";


    public  String [] tblColoumn = {ID, IdCategory , Nama,Deskripsi,Bahan,Cara_Membuat, Image};



    public ResepMakananController (Context c) {
        dbHelper = new DbController(c);

    }
    public void  open() throws  Exception{

        database= dbHelper.getWritableDatabase();

    }

    public  void close() {
        dbHelper.close();
    }

    public void deleteData() {
        database.delete(TABLE_NAME, null, null);
    }

    public  void insertData( int id , int idCategory ,  String nama, String desk, String bahan, String cara, String gbr) {
        ContentValues contentValues =  new ContentValues();
        contentValues.put(ID,id);
        contentValues.put(IdCategory,idCategory);
        contentValues.put(Nama,nama);
        contentValues.put(Deskripsi,desk);
        contentValues.put(Bahan,bahan);
        contentValues.put(Cara_Membuat,cara);
        contentValues.put(Image,gbr);
        database.insert(TABLE_NAME, null, contentValues);

    }

    public  ArrayList<ResepMakanan> getDataByCategory (int cate) {

        String categor = String.valueOf(cate);
        Log.i("INI DIA NILAINYA", "MASUK "+ categor);
        ArrayList<ResepMakanan> allData = new ArrayList<ResepMakanan>();
        Cursor cursor = null;
        cursor = database.query(TABLE_NAME, tblColoumn, IdCategory+"=?", new String[] {categor}, null,
                null, ID + " ASC");
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            allData.add(parseData(cursor));
            cursor.moveToNext();
        }
        cursor.close();
        return allData;


    }

    public ArrayList<ResepMakanan> getData() {


        ArrayList<ResepMakanan> allData = new ArrayList<ResepMakanan>();
        Cursor cursor = null;
        cursor = database.query(TABLE_NAME, tblColoumn, null, null, null,
                null, ID + " ASC");
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            allData.add(parseData(cursor));
            cursor.moveToNext();
        }
        cursor.close();
        return allData;

    }


    private ResepMakanan parseData(Cursor cursor) {
        ResepMakanan curData = new ResepMakanan();
        curData.setId_resep(cursor.getInt(0));
        curData.setId_category(cursor.getInt(1));
        curData.setNama(cursor.getString(2));
        curData.setDeskripsi(cursor.getString(3));
        curData.setBahan(cursor.getString(4));
        curData.setCara_Membuat(cursor.getString(5));
        curData.setImage(cursor.getString(6));
        return curData;
    }

}
