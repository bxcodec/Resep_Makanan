package up.tech.bukuresepmakanan11113064;

import android.app.Application;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;

import up.tech.bukuresepmakanan11113064.R;

/**
 * Created by Takiya on 13/05/2016.
 */
public class AnalyticsApplication extends Application{

    private  Tracker tracker ;

    synchronized  public  Tracker  getDefaultTracker () {

        if (tracker== null) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);


            tracker = analytics.newTracker(R.xml.global_tracker);

        }
        return tracker;
    }
}
