package up.tech.bukuresepmakanan11113064.API;

import java.util.List;

import up.tech.bukuresepmakanan11113064.Database.Model.CategoryModel;
//import up.tech.bukuresepmakanan11113064.Database.Model.ResepMakanan;

/**
 * Created by Takiya on 10/05/2016.
 */
public class POJOCategory {
    public boolean error;
    List<CategoryModel> category;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public List<CategoryModel> getResepbycategory() {
        return category;
    }

    public void setResepbycategory(List<CategoryModel> resepbycategory) {
        this.category = resepbycategory;
    }

    @Override
    public String toString() {
        return "POJOCategory{" +
                "error=" + error +
                ", resepbycategory=" + category +
                '}';
    }
}
