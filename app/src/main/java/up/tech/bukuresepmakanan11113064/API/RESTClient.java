package up.tech.bukuresepmakanan11113064.API;

import android.content.SharedPreferences;

import retrofit.RestAdapter;

/**
 * Created by Takiya on 30/04/2016.
 */


public class RESTClient {
    private static SharedPreferences preferences;
    private  static SharedPreferences.Editor editor;
    private static ResepMakananAPI REST_CLIENT;
    public static String BASE_URL = "http://192.168.43.150:8082/resepmakanan";
//    private static String BASE_URL = "http://172.35.40.64:81/resepmakanan";

    static {

        setupRestClient();
//        this.editor = this.preferences.edit();
//        setupSharedPrefer();
    }

    private RESTClient() {

    }

    public static ResepMakananAPI get() {
        return REST_CLIENT;
    }

//    private static void setupSharedPrefer() {
//        preferences= getSharedPreferen
//        editor = preferences.edit();
//        editor.putString("urlAPI", BASE_URL);
//        editor.commit();
//    }
    private static void setupRestClient() {
        RestAdapter builder = new RestAdapter.Builder().setEndpoint(BASE_URL).build();
        REST_CLIENT = builder.create(ResepMakananAPI.class);
    }

}

