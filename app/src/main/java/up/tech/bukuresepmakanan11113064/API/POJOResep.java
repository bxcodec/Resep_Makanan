package up.tech.bukuresepmakanan11113064.API;

import java.util.List;

import up.tech.bukuresepmakanan11113064.Database.Model.ResepMakanan;

/**
 * Created by Takiya on 30/04/2016.
 */
public class POJOResep {

    public boolean error;
    List<ResepMakanan>resep;

    public POJOResep() {
    }

    public POJOResep(boolean error, List<ResepMakanan> resepbycategory) {
        this.error = error;
        this.resep = resepbycategory;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public List<ResepMakanan> getResepbycategory() {
        return resep;
    }

    public void setResepbycategory(List<ResepMakanan> resepbycategory) {
        this.resep= resepbycategory;
    }


}
