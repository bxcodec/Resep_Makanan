package up.tech.bukuresepmakanan11113064.API;

import retrofit.Callback;
//import retrofit.http.POST;
//import retrofit.http.Path;
import retrofit.http.*;
/**
 * Created by Takiya on 30/04/2016.
 */
public interface ResepMakananAPI {
//    @FormUrlEncoded
//    @POST("/resepbycategory")
//    void getResepByCategory(@Field("category") int category , Callback<POJOResep> callback );

    @GET("/category")
    void getAllCategory(Callback<POJOCategory> response) ;

    @GET("/allresep")
    void getAllResep (Callback<POJOResep> resepCallback);
}
