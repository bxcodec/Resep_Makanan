package up.tech.bukuresepmakanan11113064.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;

import java.util.ArrayList;

import up.tech.bukuresepmakanan11113064.Resep.ResepActivity;

/**
 * Created by Takiya on 20/03/2016.
 */
public class GridDrawerClickListener implements AdapterView.OnItemClickListener {

    ArrayList<PacketData> listItem;
    Context mcontex;

    public GridDrawerClickListener(Context context, ArrayList<PacketData> data) {
        listItem = data;
        mcontex = context;

    }
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//        Log.i("IMAN GANTENG", "WOKEEE");
        Intent intent  = new Intent(mcontex, ResepActivity.class);
        intent.putExtra("dataToView",listItem.get(position));
        mcontex.startActivity(intent);
    }

    /*rotected void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        try {
            Class classe = Class.forName("com.google.GridViewDemo." + items[position]);
            Intent i = new Intent(this, classe);
            startActivity(i);*/
}
