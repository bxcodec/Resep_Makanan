package up.tech.bukuresepmakanan11113064;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;

import up.tech.bukuresepmakanan11113064.adapter.ListViewAdapterResep;
import up.tech.bukuresepmakanan11113064.adapter.ListViewClickListener;
import up.tech.bukuresepmakanan11113064.adapter.PacketData;

public class MenuresepActivity extends AppCompatActivity {

    ArrayList <PacketData> data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menuresep);

        isiData();
        ListView list = (ListView) findViewById(R.id.listMenu);
        list.setAdapter(new ListViewAdapterResep(data,this));
        list.setOnItemClickListener(new ListViewClickListener(this,data));
//        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                Log.i("ONCLICK MAMMEN", "MASUK MAMMMEN");
//            }
//        });

//        ls

    }





    String labl []= {"Resep Umum", "Resep Ayam","Resep Minuman","Resep Sambal", "Resep Kue",
    "Resep Praktis", "Resep Khas"
    };

    int image[] = {
            R.drawable.umum,R.drawable.ayam,R.drawable.minuman, R.drawable.sambal,
            R.drawable.kue,R.drawable.praktis,R.drawable.khas
    };
    void isiData(){
        data = new ArrayList<PacketData>();
        PacketData dataTemp;
        for(int i=0; i<labl.length; i++){
            dataTemp = new PacketData();
            dataTemp.setLbl(labl[i]);
            dataTemp.setIdData(image[i]);
            data.add(dataTemp);
        }

    }

}
