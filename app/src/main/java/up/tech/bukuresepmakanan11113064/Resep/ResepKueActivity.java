package up.tech.bukuresepmakanan11113064.Resep;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.lang.reflect.Field;
import java.util.ArrayList;

import up.tech.bukuresepmakanan11113064.Database.Controller.CategoryController;
import up.tech.bukuresepmakanan11113064.Database.Controller.ResepMakananController;
import up.tech.bukuresepmakanan11113064.Database.Model.CategoryModel;
import up.tech.bukuresepmakanan11113064.Database.Model.ResepMakanan;
import up.tech.bukuresepmakanan11113064.R;
import up.tech.bukuresepmakanan11113064.AnalyticsApplication;
import up.tech.bukuresepmakanan11113064.adapter.AdapterResepViewItem;
import up.tech.bukuresepmakanan11113064.adapter.PacketData;

public class ResepKueActivity extends AppCompatActivity {

//    ArrayList<ResepMakanan> data;
//    ResepMakananController db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resep_kue);
        TextView credit = (TextView) findViewById(R.id.credits);
        credit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bukaBrowser("http://bxcode-rev.blogpsot.com");
            }
        });

//        isiView();
        TextView Web = (TextView) findViewById(R.id.webSite);
        Web.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bukaBrowser("http://google.com");
            }
        });


        db = new ResepMakananController(this);
        categoryController = new CategoryController(this);

        data = new ArrayList<ResepMakanan>();
        isiView();
        ListView listView =(ListView) findViewById(R.id.pilihanResep);
        listView.setAdapter(new AdapterResepViewItem(data,this));

        AnalyticsApplication appl = (AnalyticsApplication) getApplication();
        tracker = appl.getDefaultTracker();

    }
    private String name = "Resep Kue";
    Tracker tracker;

    @Override
    protected void onResume() {
        super.onResume();
        Log.i("RESEP AYAM", "Setting screen name: " + name);
        tracker.setScreenName("Image~" + name);
        tracker.send(new HitBuilders. ScreenViewBuilder().build());
    }
    ArrayList<ResepMakanan> data;
    CategoryController  categoryController;
    ResepMakananController db ;

    void isiView() {


        try {

            categoryController.open();
            CategoryModel model = categoryController.getCategoryByName("Resep Kue");
            Log.i("YANG DIAMBIL", model.toString());
            db.open();

//            ArrayList<ResepMakanan> test = db.getData();
//            Log.i("TOTAL DATA", "SEKIAN "+ test.size());
            data = db.getDataByCategory(model.getId_category());

        } catch (Exception e) {
            Log.e("ERROR", "DATABASE ERROR", e);
        }
        finally {
            Log.i("ACTIVITY AYAM", "INI DIA AYAM 3 ="+ data.size());
            db.close();
            categoryController.close();
        }

    }

    private void bukaBrowser (String alamat) {

        Uri uri =  Uri.parse(alamat);
        Intent i = new Intent(Intent.ACTION_VIEW,uri);
        startActivity(i);
    }


    private ArrayList<PacketData> getAllResourceIDs(Class<?> aClass) throws IllegalArgumentException{
                /* Get all Fields from the class passed. */
        Field[] IDFields = aClass.getFields();

//        int[] IDs = new int[IDFields.length];
        PacketData [] data = new PacketData[IDFields.length];
        ArrayList<PacketData> list = new ArrayList<PacketData>();
        PacketData tempData =null;
        try {
                        /* Loop through all Fields and store id to array. */
            for(int i = 0; i < IDFields.length; i++){
                                /* All fields within the subclasses of R
                                 * are Integers, so we need no type-check here. */

                // pass 'null' because class is static


                String [] spliter = IDFields[i].getName().split("_");
                if(spliter[0].compareToIgnoreCase("kue")==0 && spliter.length==2) {
                    tempData = new PacketData();
                    tempData.setIdData(IDFields[i].getInt(null));
                    tempData.setLbl(spliter[1]);
//                    data[i] = new PacketData(tempData);
                    list.add(  tempData);
//                    Log.v("DALAM LOOP", tempData.toString());
                }
            }
        } catch (Exception e) {
                        /* Exception will only occur on bad class submitted. */
            throw new IllegalArgumentException();
        }

//        for(int K=0; K<data.length; K++)
//        {
////           data[K] = new PacketData();
////            data[K].lbl = list.get(K).lbl;
////            data[K].idData = list.get(K).idData;
//
//            Log.v("IMAN SAYAH",data[K].toString());
//        }
        return list;

    }


}
