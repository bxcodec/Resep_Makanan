package up.tech.bukuresepmakanan11113064.Resep;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import up.tech.bukuresepmakanan11113064.Database.Model.ResepMakanan;
import up.tech.bukuresepmakanan11113064.R;
import up.tech.bukuresepmakanan11113064.AnalyticsApplication;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.squareup.picasso.Picasso;

public class ResepActivity extends AppCompatActivity {

    private static SharedPreferences preferences;
//    private  static SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.resep_list_view);

        ResepMakanan data = (ResepMakanan) getIntent().getSerializableExtra("data");
        TextView nama  = (TextView) findViewById(R.id.namaResep);
        TextView deskripsi= (TextView) findViewById(R.id.deskripsiResep);
        TextView bahan  = (TextView) findViewById(R.id.bahanResep);
        TextView cara  = (TextView) findViewById(R.id.caraMembuat);
        ImageView gbr  = (ImageView) findViewById(R.id.resepGambar);

        preferences = getSharedPreferences("URLAPI",MODE_PRIVATE);

        String url  = preferences.getString("url","-");

        name+= data.getNama();
        nama.setText(data.getNama());
        deskripsi.setText(data.getDeskripsi());
        bahan.setText(data.getBahan());
        cara.setText(data.getCara_Membuat());
        Log.i("URL GAMBAR", "url : " + url+ data.getImage());
//        Picasso.with(this).load(url+data.getImage()).resize(100,100).into(gbr);
        Picasso.with(this)
                .load(""+url+"/"+data.getImage())
                .placeholder(R.drawable.logo)
                .error(R.drawable.logo)
                .resize(100,100)
                .centerCrop().into(gbr);

        AnalyticsApplication appl = (AnalyticsApplication) getApplication();
        tracker = appl.getDefaultTracker();

    }
    private String name = "Resep ";
    Tracker tracker;

    @Override
    protected void onResume() {
        super.onResume();
        Log.i("RESEP AYAM", "Setting screen name: " + name);
        tracker.setScreenName("Image~" + name);
        tracker.send(new HitBuilders. ScreenViewBuilder().build());
    }


//    AsyncTask
}
